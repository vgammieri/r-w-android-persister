package com.reactivework.android.persitent.entities;

import java.util.Iterator;

import android.database.Cursor;

import com.reactivework.android.persitent.ex.PreconditionViolatedException;

public class EntityIterator<E extends Entity> implements Iterator<E> {
 
	private Cursor c = null;
	private Class<E> clazz;
	
	public EntityIterator(Class<E> clazz, Cursor c) {
		this.clazz = clazz;
		this.c = c;
	}
	
	@Override
	public boolean hasNext() {
		return c.isLast() == false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E next() {
		boolean moveToNext = this.c.moveToNext();
		if (moveToNext == false){
			throw new PreconditionViolatedException();
		}
		
		E newInstance;
		try {
			newInstance = clazz.newInstance();
		} catch (Exception e) {
			throw new PreconditionViolatedException(e);
		}
		Entity fromCursor = newInstance.fromCursor(c);
		return (E) fromCursor;
	}

	@Override
	public void remove() {
		c.moveToNext();
		
	}

}
