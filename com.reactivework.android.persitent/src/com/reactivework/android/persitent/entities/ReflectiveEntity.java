/** This file is part of R/W Android Persiter.

    R/W Android Persiter is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R/W Android Persiter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with R/W Android Persiter.  If not, see <http://www.gnu.org/licenses/>.
    
    Vincenzo Gammieri <vincenzo.gammieri@reactivework.com>
    
 */

package com.reactivework.android.persitent.entities;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.reactivework.android.persitent.ex.FatalErrorException;

public class ReflectiveEntity extends Entity {

	private static final long serialVersionUID = 1L;
	
	private Map<String, Class<?>> fieldTypes = null;

	private static String TAG = ReflectiveEntity.class.getCanonicalName();

	protected ReflectiveEntity() {

	}

	private void calcFiledTypes() {

		if (fieldTypes != null) {
			return;
		}

		fieldTypes = new HashMap<String, Class<?>>();

		Class<? extends ReflectiveEntity> thisClass = this.getClass();
		Field[] fields = thisClass.getDeclaredFields();

		for (Field field : fields) {
			String name = field.getName();
			String upperCase = name.substring(0, 1).toUpperCase()
					+ name.substring(1);
			String getter = "get" + upperCase;
			String setter = "set" + upperCase;
			Method methodGetter = null;
			try {
				methodGetter = thisClass.getMethod(getter);
			} catch (NoSuchMethodException e) {

			}
			Method methodSetter = null;
			try {
				methodSetter = thisClass.getMethod(setter, field.getType());
			} catch (NoSuchMethodException e) {

			}

			if (methodGetter != null && methodSetter != null) {
				fieldTypes.put(name, field.getType());
			}
		}

	}

	@Override
	public void onCreate(SQLiteDatabase db) throws SecurityException {
		calcFiledTypes();

		StringBuilder b = new StringBuilder();
		b.append("create table " + getTableName()
				+ " (_id integer primary key,");

		for (String fieldName : fieldTypes.keySet()) {

			if (fieldName.equals("id")) {
				b.append("");
			} else {
				String suffix = " ";
				String type;
				Class fieldClass = fieldTypes.get(fieldName);
				if (fieldClass.equals(Integer.class)
						|| fieldClass.equals(int.class)) {
					type = "integer";
				} else if (fieldClass.equals(Long.class)
						|| fieldClass.equals(long.class)) {
					type = "integer";
				} else if (fieldClass.equals(String.class)) {
					type = "text";
				} else if (fieldClass.isArray()) {
					// type = "clob";
					throw new UnsupportedOperationException();
				} else if (Entity.class.isAssignableFrom(fieldClass)) {
					type = "integer";
					suffix = "_FK ";
				} else {
					type = "clob";
				}

				b.append("_");
				b.append(fieldName);
				b.append(suffix);
				b.append(type);
				b.append(",");
			}
		}
		b.deleteCharAt(b.length() - 1);
		b.append(")");

		String sql = b.toString();
		Log.d(TAG, "SQL: " + sql);

		db.execSQL(sql);

	}

	@Override
	public ContentValues toContentValues() {
		ContentValues contentValues = new ContentValues();
		calcFiledTypes();

		Class<? extends ReflectiveEntity> thisClass = this.getClass();

		for (String name : fieldTypes.keySet()) {

			String upperCase = name.substring(0, 1).toUpperCase()
					+ name.substring(1);
			String getter = "get" + upperCase;

			Method methodGetter = null;
			try {
				methodGetter = thisClass.getMethod(getter);
			} catch (NoSuchMethodException e) {
				throw new FatalErrorException(e);
			}

			try {
				Object invoke = methodGetter.invoke(this);
				Class<? extends Object> resultClass = invoke.getClass();

				if (resultClass.equals(Integer.class)
						|| resultClass.equals(int.class)) {

					contentValues.put("_" + name, (Integer) invoke);

				} else if (resultClass.equals(Long.class)
						|| resultClass.equals(long.class)) {

					contentValues.put("_" + name, (Long) invoke);

				} else if (resultClass.equals(String.class)) {

					contentValues.put("_" + name, (String) invoke);

				} else if (Entity.class.isAssignableFrom(resultClass)) {

					throw new UnsupportedOperationException("Entity relations are not yet supported");

				} else {
					throw new UnsupportedOperationException("Type are not yet supported");
				}

			} catch (Exception e) {
				throw new FatalErrorException(e);
			}

		}
		return contentValues;
	}

	@Override
	public Entity fromCursor(Cursor cursor) {
		calcFiledTypes();

		Class<? extends ReflectiveEntity> thisClass = this.getClass();

		try {
			ReflectiveEntity newInstance = thisClass.newInstance();
			
			long id = cursor.getLong(cursor.getColumnIndexOrThrow(COL_ID));
			
			newInstance.setId(id);
			
			for (String name : fieldTypes.keySet()) {

				Class<?> resultClass = fieldTypes.get(name);

				String columnName = "_" + name;

				int columnIndex = cursor.getColumnIndexOrThrow(columnName);

				Object result = null;

				if (resultClass.equals(Integer.class)
						|| resultClass.equals(int.class)) {

					result = cursor.getInt(columnIndex);

				} else if (resultClass.equals(Long.class)
						|| resultClass.equals(long.class)) {

					result = cursor.getLong(columnIndex);

				} else if (resultClass.equals(String.class)) {

					result = cursor.getString(columnIndex);

				} else {
					throw new UnsupportedOperationException("Type not yet supported");
				}
				
				Method method = thisClass.getMethod("set"+name.substring(0,1).toUpperCase()+name.substring(1), resultClass);
				method.invoke(newInstance, result);

			}
			return newInstance;
		} catch (Exception e) {
			throw new FatalErrorException(e);
		}
	}

}
