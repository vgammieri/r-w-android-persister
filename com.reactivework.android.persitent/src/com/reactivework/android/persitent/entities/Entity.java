/** This file is part of R/W Android Persiter.

    R/W Android Persiter is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R/W Android Persiter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with R/W Android Persiter.  If not, see <http://www.gnu.org/licenses/>.
    
    Vincenzo Gammieri <vincenzo.gammieri@reactivework.com>
*/
package com.reactivework.android.persitent.entities;

import java.io.Serializable;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public abstract class Entity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final String COL_ID = "_id";
	
	public abstract void onCreate(SQLiteDatabase db);
	
	public String getTableName(){
		return this.getClass().getSimpleName();
	}
	
	private long id ;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public abstract ContentValues toContentValues();
	public abstract Entity fromCursor(Cursor cursor);
	
}
