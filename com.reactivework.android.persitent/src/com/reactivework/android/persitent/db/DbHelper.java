/** This file is part of R/W Android Persiter.

    R/W Android Persiter is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R/W Android Persiter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with R/W Android Persiter.  If not, see <http://www.gnu.org/licenses/>.
    
    Vincenzo Gammieri <vincenzo.gammieri@reactivework.com>
 */
package com.reactivework.android.persitent.db;

import java.util.HashSet;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.reactivework.android.persitent.entities.Entity;
import com.reactivework.android.persitent.ex.DataNotFoundException;
import com.reactivework.android.persitent.ex.FatalErrorException;

public class DbHelper extends SQLiteOpenHelper {

	private static final String TAG = DbHelper.class.getCanonicalName();

	private Set<Class<? extends Entity>> registeredClasses = new HashSet<Class<? extends Entity>>();

	private String[] selectionArgs;

	public DbHelper(Context context, String dbName, int dbVersion) {
		super(context, dbName, null, dbVersion);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		Log.d(TAG, "Create DB");

		db.beginTransaction();

		for (Class<?> c : this.registeredClasses) {
			try {
				Entity newInstance = (Entity) c.newInstance();
				newInstance.onCreate(db);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
				throw new FatalErrorException(e);
			}
		}

		db.setTransactionSuccessful();
		db.endTransaction();

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(TAG, "onUpgrade");

	}

	public long insert(Entity entity) {
		ContentValues values = entity.toContentValues();
		long key = this.getWritableDatabase().insert(entity.getTableName(),
				null, values);
		entity.setId(key);
		return key;
	}

	public long update(Entity entity) {

		ContentValues values = entity.toContentValues();
		long num = this.getWritableDatabase().update(entity.getTableName(),
				values, "_id = ?",
				new String[] { String.valueOf(entity.getId()) });

		return num;
	}

	public long delete(Entity entity) {
		return this.getWritableDatabase().delete(entity.getTableName(),
				"_id = ?", new String[] { String.valueOf(entity.getId()) });
	}

	public Cursor selectAll(Class<? extends Entity> class1, String orderBy) {
		return this.getReadableDatabase().query(class1.getSimpleName(), null,
				null, null, null, null, orderBy);
	}

	public <T extends Entity> T loadAndCloseDb(Class<T> class1, long id)
			throws DataNotFoundException {
		try {
			T load = this.load(class1, id);
			return load;
		} catch (DataNotFoundException e) {
			throw e;
		} finally {
			this.close();
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> T load(Class<T> class1, long id)
			throws DataNotFoundException {
		Cursor cursor = this.getReadableDatabase().query(
				class1.getSimpleName(), null, "_id = ?",
				new String[] { String.valueOf(id) }, null, null, null);
		if (cursor.moveToFirst() == false) {
			throw new DataNotFoundException();
		}
		try {
			T newInstance = class1.newInstance();
			Entity fromCursor = newInstance.fromCursor(cursor);
			return (T) fromCursor;
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG,
					"Unable to instantiate class " + class1.getCanonicalName());
			throw new RuntimeException(e);
		} finally {
			if (cursor != null)
				cursor.close();
		}

	}

	public long updateAndCloseDb(Entity gerepo) {
		long update = this.update(gerepo);
		close();
		return update;
	}

	public void registerEntity(Class<? extends Entity> class1) {
		this.registeredClasses.add(class1);

	}

	public Cursor query(Class<? extends Entity> clazz, String selection,
			String groupBy, String having, String orderBy) {
		Cursor query = this.getReadableDatabase().query(clazz.getSimpleName(),
				null, selection, selectionArgs, groupBy, having, orderBy);
		return query;
	}

	public Cursor query(Class<? extends Entity> clazz, String selection,
			String orderBy) {
		return this.query(clazz, selection, null, null, orderBy);
	}

}
