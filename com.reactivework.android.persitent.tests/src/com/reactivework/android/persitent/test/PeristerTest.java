/** This file is part of R/W Android Persiter.

    R/W Android Persiter is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R/W Android Persiter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with R/W Android Persiter.  If not, see <http://www.gnu.org/licenses/>.
    
    Vincenzo Gammieri <vincenzo.gammieri@reactivework.com>
 */
package com.reactivework.android.persitent.test;

import android.database.Cursor;

import com.reactivework.android.persitent.db.DbHelper;
import com.reactivework.android.persitent.entities.EntityIterator;
import com.reactivework.android.persitent.ex.DataNotFoundException;

public class PeristerTest extends android.test.AndroidTestCase {
	private DbHelper dbHelper;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		getContext().deleteDatabase("TEST");
		this.dbHelper = new DbHelper(getContext(), "TEST", 1);
		this.dbHelper.registerEntity(TestEntity.class);
		this.dbHelper.registerEntity(ReflectiveTestEntity.class);

	}

	public void testContextStart() {

	}

	public void testCreateAndLoadEntity() throws DataNotFoundException {
		TestEntity entity = new TestEntity();
		this.dbHelper.insert(entity);
		TestEntity entity2 = this.dbHelper.loadAndCloseDb(TestEntity.class,
				entity.getId());
		assertEquals(entity.getId(), entity2.getId());

	}

	public void testReflectiveEntity() {
		ReflectiveTestEntity entity = new ReflectiveTestEntity();
		entity.setIntField(10);
		entity.setLongField(100L);
		entity.setStringField("xxxx");
		
		this.dbHelper.insert(entity);
	}

	public void testIterator() {
		ReflectiveTestEntity entity = new ReflectiveTestEntity();
		entity.setIntField(10);
		entity.setLongField(100L);
		entity.setStringField("xxxx");
		
		this.dbHelper.insert(entity);

		ReflectiveTestEntity entity2 = new ReflectiveTestEntity();
		entity2.setIntField(100);
		entity2.setLongField(1000L);
		entity2.setStringField("xxxx0");
		
		this.dbHelper.insert(entity2);

		Cursor query = this.dbHelper.query(ReflectiveTestEntity.class, null,
				ReflectiveTestEntity.COL_ID);
		
		EntityIterator<ReflectiveTestEntity> iterator = new EntityIterator<ReflectiveTestEntity>(
				ReflectiveTestEntity.class, query);

		int c = 0;
		while(iterator.hasNext()){
			ReflectiveTestEntity next = iterator.next();
			assertTrue(next.getId() == entity.getId() || next.getId() == entity2.getId());
			++c;
		}
		
		assertEquals(2, c);
	}

	@Override
	protected void tearDown() throws Exception {
		dbHelper.close();
		super.tearDown();

	}

}
