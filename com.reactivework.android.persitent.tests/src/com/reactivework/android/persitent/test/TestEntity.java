/** This file is part of R/W Android Persiter.

    R/W Android Persiter is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    R/W Android Persiter is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with R/W Android Persiter.  If not, see <http://www.gnu.org/licenses/>.
    
    Vincenzo Gammieri <vincenzo.gammieri@reactivework.com>
*/
package com.reactivework.android.persitent.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.reactivework.android.persitent.entities.Entity;

public class TestEntity extends Entity{

	private static final long serialVersionUID = 1L;

	@Override
	public void onCreate(SQLiteDatabase db) {
		String q = "create table "+getTableName()+" (_id integer primary key)";
		db.execSQL(q);
		
	}


	@Override
	public ContentValues toContentValues() {
		ContentValues res = new ContentValues();
		res.put(COL_ID, this.getId());
		return res;
	}

	@Override
	public Entity fromCursor(Cursor cursor) {
		long id = cursor.getLong(cursor.getColumnIndexOrThrow(COL_ID));
		TestEntity entity = new TestEntity();
		entity.setId(id);
		return entity;
	}

}
